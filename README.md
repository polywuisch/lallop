# lallop

Implementation of the lallop algorithm (in Haskell) for estimating the simplicial volume 
of one-relator groups (N. Heuer, C. L&ouml;h, arXiv: 1911.02470 [math.GT])

## Explanation of the algorithm

[arXiv preprint] (https://arxiv.org/abs/1911.02470)

## Used solvers 

z3 (default), glpk (optional)

## Documentation for this implementation

[haddock documentation] (http://www.mathematik.uni-regensburg.de/loeh/lallop/doc/index.html)
