-----------------------------------------------------------------------------
-- |
-- Module      :  Relator.hs
-- Copyright   :  (c) Nicolaus Heuer, Clara Löh
-- License     :  BSD3
-- Maintainer  :  clara.loeh@mathematik.uni-r.de
-- Stability   :  experimental
--
-- Implementation of lallop (an LP-based bound for simplicial volume
-- of one-relator groups, arXiv: 1911.02470 [math.GT]); via z3
-----------------------------------------------------------------------------


module Relator where

import Data.Char
import Data.SBV
import qualified Data.Map.Strict as M

-- | Implementation of lallop (an LP bound for simplicial volume
-- of one-relator groups, arXiv: 1911.02470 [math.GT]). Relators
-- are modelled as strings; the letters are modelled via their
-- index and sign.

-------------------------------------------------------------------
-- * Signs
-------------------------------------------------------------------

-- | a simple sign type
data Sign = Plus
          | Minus
     deriving Eq      
allSigns = [Plus, Minus] :: [Sign]

instance Show Sign where
  show Plus  = "+"
  show Minus = "-"

instance Ord Sign where
  compare Plus Minus = GT
  compare Minus Plus = LT
  compare _ _ = EQ

flipSign :: Sign -> Sign
flipSign Plus  = Minus
flipSign Minus = Plus

-- | convert Plus to 1, and Minus to -1
fromSign :: Num a => Sign -> a
fromSign Plus  =  1
fromSign Minus = -1

-------------------------------------------------------------------
-- * Relators
-------------------------------------------------------------------

-- | relators (non-trivial, in commutator subgroup)
type Relator = String

-- | flip lower/upper case (only works on a-zA-Z)
flipUpper :: Char -> Char
flipUpper x = if isUpper x then toLower x
                           else toUpper x

-- | check whether the given relator is cyclically reduced (only works on a-zA-Z)
isCycReduced :: Relator -> Bool
isCycReduced []  = True
isCycReduced [x] = True
isCycReduced r   = isReduced r && ( head r /= flipUpper (last r) ) 

-- | check whether the given word is reduced (only works on a-zA-Z)
isReduced :: Relator -> Bool
isReduced []       = True
isReduced [x]      = True
isReduced (x:y:xs) = (x /= flipUpper y) && (isReduced (y:xs))

-- | check whether the relator consists only of a-zA-Z
--                         and whether it is cyclically reduced
isValidRelator :: String -> Bool
isValidRelator r = all (\x -> isAscii x && isLetter x) r
                && isCycReduced r

-- | check whether the relator is a non-trivial element of the commutator subgroup
-- (assumes that the relator satisfies @isValidRelator@)
isComm :: String -> Bool
isComm "" = False
isComm r  = and [ length (filter (== x) r) == length (filter (== (flipUpper x)) r)
                | x <- ['a' .. 'z']
                ]

-------------------------------------------------------------------
-- * Minimal roots of relators
-------------------------------------------------------------------

-- | determines whether the given cyclically reduced word
--   is a proper power
isProperPower :: Relator -> Bool
isProperPower r = let k = snd (minimalRoot r)
                  in  k /= 1

-- | the root length of a cyclically reduced relator
--   (i.e., the length of a minimal root)
rootLength :: Relator -> Int
rootLength r = length rr
  where (rr, _) = minimalRoot r

-- | determines the minimal root (i.e., with maximal exponent)
--   of a cyclically reduced word, together with its exponent
minimalRoot :: Relator -> (Relator, Int)
minimalRoot [] = ([],1)
minimalRoot r  = case roots r of
                      []     -> error "minimalRoot: something went wrong (every relator has a minimal root!)"
                      (rr:_) -> (rr, length r `div` length rr) 

-- | compute the list of all roots
roots :: Relator -> [Relator]
roots [] = [[]]
roots r  = let n = length r
           in [ take j r
              | j <- [1 .. n]
              , n `mod` j == 0
              , isRoot r j
              ]

-- | checks whether the initial part of given length is a root
isRoot :: Relator -> Int -> Bool
isRoot [] _ = True
isRoot r  j = let k   = length r `div` j
                  rr  = take j r
                  rrj = concat $ replicate k rr
              in r == rrj

-------------------------------------------------------------------
-- * Letters
-------------------------------------------------------------------

-- | letters from the relator (with sign)
type Letter = (Int, Sign)

-- | evaluation function for letters
value :: Relator -> Letter -> Char
value r (i, Plus)  = r !! i
value r (i, Minus) = flipUpper (r !! i)

