-----------------------------------------------------------------------------
-- |
-- Module      :  Main.hs
-- Copyright   :  (c) Nicolaus Heuer, Clara Löh
-- License     :  BSD3
-- Maintainer  :  clara.loeh@mathematik.uni-r.de
-- Stability   :  experimental
--
-- Executable for:
-- Implementation of lallop (an LP-based bound for simplicial volume
-- of one-relator groups, arXiv: 1911.02470 [math.GT])
-----------------------------------------------------------------------------

module Main where

import Data.Char
import Data.SBV
import qualified Data.SBV.Internals as I 
import qualified Data.Map.Strict as M
import System.Environment
import System.Exit
import System.IO
import System.Console.GetOpt

-- import lallop-specific modules
import Relator
import RootLallop     -- setup of pods etc; default solver via z3
import RootLallopGLPK -- version via glpk

-------------------------------------------------------------------
-- * Main
-- Executable for Lallop
-------------------------------------------------------------------

-------------------------------------------------------------------
-- ** Argument and option handling

-- | command line flags
data Flag
  = Scl  -- ^ compute scl instead of lallop
  | Log  -- ^ log to lallop.log
  | GLPK -- ^ numerical solution via GLPK instead of exact solution via z3 
  deriving (Eq,Show)

allOptions :: [OptDescr Flag]
allOptions =
    [ Option ['l'] ["log"]  (NoArg Log)  "log to lallop.log (z3 only)"
    , Option ['g'] ["glpk"] (NoArg GLPK) "numerical solution via GLPK instead of exact solution via z3"
    , Option ['s'] ["scl"]  (NoArg Scl)  "compute scl instead of lallop"
    ]

inputOpts :: [String] -> IO ([Flag], [String])
inputOpts argv = 
     case getOpt Permute allOptions argv of
         (o,r,[]  ) -> return (o,r)
         (_,_,errs) -> do
                        hPutStrLn stderr (concat errs ++ usageInfo lallopInfo allOptions)
                        exitFailure

lallopInfo = "Usage: lallop [OPTION...] <relator> (e.g., abAB)"

-- | z3 options
z3Opts :: [Flag] -> SMTConfig 
z3Opts fs = if Log `elem` fs
            then z3 { optimizeValidateConstraints = True
                    , verbose                     = True
                    , redirectVerbose             = Just "lallop.log"
                    }
            else z3 { optimizeValidateConstraints = True
                    }

-- | the different goal types
data LPTypes
  = Lallop
  | Scallop
  deriving (Eq,Show)

selectGoal :: [Flag] -> LPTypes 
selectGoal fs = if Scl `elem` fs
                     then Scallop
                     else Lallop

-- | the different solver types
data SolverTypes
  = S_Z3   -- ^ z3 (default solver)
  | S_GLPK -- ^ GLPK

selectSolver :: [Flag] -> SolverTypes
selectSolver fs = if GLPK `elem` fs
                       then S_GLPK
                       else S_Z3

-- | call the corresponding solver to compute lallop or scl
solveGoal :: [Flag] -> SolverTypes -> LPTypes -> Relator -> IO ()
solveGoal opts S_Z3   Lallop r  = do
                                   x <- optimizeWith (z3Opts opts) Lexicographic (lallopLP r)
                                   putStrLn $ show (selectNonzeroResults "lallop" x)
solveGoal opts S_Z3   Scallop r = do
                                   x <- optimizeWith (z3Opts opts) Lexicographic (scallopLP r)
                                   putStrLn $ show (selectNonzeroResults "scallop" x)
solveGoal _    S_GLPK Lallop r  = putStrLn $ "lallop: " ++ show (lallopGLPK r)
solveGoal _    S_GLPK Scallop r = putStrLn $ "scl:    " ++ show (scallopGLPK r)


missingArgumentErr :: IO ()
missingArgumentErr
  = do 
      hPutStrLn stderr "missing argument: cyclically reduced relator, e.g. abAB"
      hPutStrLn stderr (usageInfo lallopInfo allOptions)
      exitFailure

malformedRelatorErr :: String -> IO ()
malformedRelatorErr r
  = do
      hPutStrLn stderr "the relator may only consist of a-zA-Z and has to be cyclically reduced"
      hPutStrLn stderr ("input relator: " ++ show r)
      exitFailure

notCommRelatorErr :: String -> IO ()
notCommRelatorErr r
  = do
      hPutStrLn stderr "the relator is not a non-trivial element of the commutator subgroup"
      hPutStrLn stderr ("input relator: " ++ show r)
      exitFailure

-------------------------------------------------------------------
-- ** main

-- | check whether the given @CV@ is a (CAlg) real number and equal to zero
isZero :: I.CV -> Bool
isZero (I.CV _ (I.CAlgReal x)) = x == 0
isZero _                       = False

-- | select only those variables in the result that are non-zero
selectNonzeroResults :: String -> OptimizeResult -> OptimizeResult
selectNonzeroResults f x
   = case x of
       LexicographicResult (Satisfiable s y) -> let z = filter (\(s, v)  -> (not $ isZero v)
                                                                         || (s == f))
                                                               (I.modelAssocs y)
                                                in  (LexicographicResult (Satisfiable s y {I.modelAssocs = z}))
       _                                     -> x

main :: IO ()
main = do
         inputArgs <- getArgs
         (opts, args) <- inputOpts inputArgs
         case args of
           []    -> missingArgumentErr
           (r:_) -> do
                      if isValidRelator r -- is r a String of a-zA-Z ?
                         then if isComm r -- is r in the commutator subgroup and non-trivial?
                                 then solveGoal opts (selectSolver opts) (selectGoal opts) r
                                 else notCommRelatorErr r
                         else malformedRelatorErr r

