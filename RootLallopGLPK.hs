-----------------------------------------------------------------------------
-- |
-- Module      :  RootLallopGLPK.hs
-- Copyright   :  (c) Nicolaus Heuer, Clara Löh
-- License     :  BSD3
-- Maintainer  :  clara.loeh@mathematik.uni-r.de
-- Stability   :  experimental
--
-- Implementation of lallop (an LP-based bound for simplicial volume
-- of one-relator groups, arXiv: 1911.02470 [math.GT]); using hmatrix-glpk;
-- the results are only numerical, not exact
-----------------------------------------------------------------------------


module RootLallopGLPK where

import Relator
import RootLallop
import Data.Char
import Numeric.LinearProgramming
import qualified Data.Map.Strict as M

-- | Implementation of lallop (an LP bound for simplicial volume
-- of one-relator groups, arXiv: 1911.02470 [math.GT]); using hmatrix-glpk

-------------------------------------------------------------------
-- * Basic constraints for the LP (A_0^\Q)
-------------------------------------------------------------------
-------------------------------------------------------------------
-- * Passing the lallop LP to hmatrix-glpk (whence to GLPK)
-------------------------------------------------------------------

lallopGLPK :: Relator -> Double
lallopGLPK r
  = let solv = exact (Minimize (lallopP r)) (lallopC r) []
    in case solv of
          Optimal (x , _) -> x
          Unbounded       -> error "There is no bounded solution."
          _               -> error "There is a problem with the problem (unsatisfiable?)." 

-- | rewriting the lallop functional as a list of values on the basis
lallopP :: Relator -> [Double]
lallopP r = map lallopf (basis r)

-- | for the library, we need to give variables numbers;
-- we use the enumeration given by @basis@
nBasis :: Relator -> [(Pod,Int)]
nBasis r = zip (basis r) [1..] 


-- the lallop constraints;
-- the constraint "x >= 0" is already included by default!
lallopC :: Relator -> Constraints
lallopC r = Sparse (   powersM
                   ++  fstsndM
                   ++  invM
                   )
  where
    b = nBasis r
    -- normalisation of powers/degrees
    powersM    = [ [ (nu p) # j | (p,j) <- b ] :==: 1
                 ]

    -- A_0-constraints
    -- all coeffs >= 0 (already built-in default)

    -- occurrences of rectangle pairs in fst component == occs ... in snd component
    fstsndM    = [ [ (fstsndConstraint rp p) # j | (p,j) <- b ] :==: 0
                 | rp <- rectanglePairs r
                 ]
    -- occurrences of rectangles == occurences of their involution
    invM       = [ [ (involutionConstraint rect p) # j | (p,j) <- b ] :==: 0
                 | rect <- rectangles r
                 ]           
          
-------------------------------------------------------------------
-- * For comparison: scallop in the same style
-------------------------------------------------------------------

-- rewriting the scallop functional as list of values on the basis
scallopP :: Relator -> [Double]
scallopP r = map scallopf (basis r)

scallopGLPK :: Relator -> Double
scallopGLPK r
  = let solv = exact (Minimize (scallopP r)) (lallopC r) []
    in case solv of
          Optimal (x , _) -> x
          Unbounded       -> error "There is no bounded solution."
          _               -> error "There is a problem with the problem (unsatisfiable?)." 

