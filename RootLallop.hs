-----------------------------------------------------------------------------
-- |
-- Module      :  RootLallop.hs
-- Copyright   :  (c) Nicolaus Heuer, Clara Löh
-- License     :  BSD3
-- Maintainer  :  clara.loeh@mathematik.uni-r.de
-- Stability   :  experimental
--
-- Implementation of lallop (an LP-based bound for simplicial volume
-- of one-relator groups, arXiv: 1911.02470 [math.GT]); via z3
-----------------------------------------------------------------------------


module RootLallop where

import Relator -- basic handling of relators
import Data.Char
import Data.SBV
import qualified Data.Map.Strict as M

-- | Implementation of lallop (an LP bound for simplicial volume
-- of one-relator groups, arXiv: 1911.02470 [math.GT])

-------------------------------------------------------------------
-- * Rectangles
-------------------------------------------------------------------

-- | rectangles
type Rectangle = (Letter, Letter)

-- | all rectangles associated with the given relator
rectangles :: Relator -> [Rectangle]
rectangles r
  = let n = rootLength r
    in [ ((i,s), (j,t))
       | i <- [0 .. n-1], j <- [0 .. n-1], i /= j
       , s <- allSigns, t <- allSigns
       , value r (i,s) == value r (j, (flipSign t))
       ] 

-- | signs of the first component of the given rectangle
s1 :: Num a => Relator -> Rectangle -> a
s1 r ((_,s),(_,_)) = fromSign s

-- | signs of the second component of the given rectangle
s2 :: Num a => Relator -> Rectangle -> a
s2 r ((_,_),(_,s)) = fromSign s

-- | sum of the signs of the components the given rectangle
s12 :: Num a => Relator -> Rectangle -> a
s12 r rect = s1 r rect + s2 r rect

-- | rectangle involution (corresponds to change of orientation)
involution :: Rectangle -> Rectangle
involution (x,y) = (y,x) 

-- | pretty-print rectangles
showRectangle :: Rectangle -> String
showRectangle ((i,s), (j,t))
  =  show i ++ show s
  ++ show j ++ show t

-- | the first rectangle follows the second rectangle (wrt the given relator)
follows :: Relator -> Rectangle -> Rectangle -> Bool
follows r r1 r2 -- rectangle r1 follows r2
  = let (_ , x1') = r1
        (x2, _  ) = r2
    in  letterFollows r x1' x2

letterFollows :: Relator -> Letter -> Letter -> Bool
letterFollows r (i1,s1) (i2,s2)
  = let n = rootLength r
    in (i1 == (i2 + 1) `mod` n && s1 == Plus  && s2 == Plus)
    || (i2 == (i1 + 1) `mod` n && s1 == Minus && s2 == Minus)
  

-- | dual to following: cancellation of reducible parts
cancels :: Relator -> Rectangle -> Rectangle -> Bool
cancels r r1 r2 -- second side of r1 cancels with first side of r2
  = let (_ , x1') = r1
        (x2, _  ) = r2
    in letterCancels r x1' x2        

letterCancels :: Relator -> Letter -> Letter -> Bool
letterCancels r (i1,s1) (i2,s2)
  = let n = rootLength r
    in i1 `mod` n == i2 `mod` n && s1 /= s2

-- | all rectangle pairs associated with the given relator
rectanglePairs :: Relator -> [(Rectangle, Rectangle)]
rectanglePairs r
  = let rects = rectangles r
    in [ (r1, r2)
       | r1 <- rects, r2 <- rects
       ]
       
-- | rectangle pairs satisfying additional irreducibility conditions;
-- used to model the open sides of (doubly) open tripods
reducedRectanglePairs :: Relator -> [(Rectangle, Rectangle)]
reducedRectanglePairs r
  = [ (r1, r2)
    | (r1, r2) <- rectanglePairs r
    , not (follows r r1 r2)
    , not (follows r r2 r1)
    , not (cancels r r1 r2)
    , not (cancels r r2 r1) 
    ]  

-------------------------------------------------------------------
-- * Pods, built from rectangles
-------------------------------------------------------------------

-------------------------------------------------------------------
-- ** Bipods

-- | bipods (represented as pairs of rectangles)
type Bipod= (Rectangle,  Rectangle)

-- | check whether the pair of rectangles satisfies the bipod condition
isBipod :: Relator -> Bipod -> Bool
isBipod r (r1, r2)
  =  follows r r2 r1 
  && follows r r1 r2
--  && r1 <= r2 -- mild size reduction (first edge is minimal)

-- | all bipods associated with the given relator
bipods :: Relator -> [Bipod]
bipods r = filter (isBipod r) (rectanglePairs r)

showBipod :: Bipod -> String
showBipod (r1,r2)
  =  "(" ++ showRectangle r1
  ++ "," ++ showRectangle r2 ++ ")"

-------------------------------------------------------------------
-- ** Tripods

-- | tripods (represented as triples of rectangles)
type Tripod = (Rectangle, Rectangle, Rectangle)

-- | check whether the triple of rectangles satisfies the tripod condition
isTripod :: Relator -> Tripod -> Bool
isTripod r (r1,r2,r3)
  =  follows r r2 r1
  && follows r r3 r2
  && follows r r1 r3

-- | all tripods associated with the given relator
tripods :: Relator -> [Tripod]
tripods r
  = [ (r1,r2,r3)
    | r1 <- rects, r2 <- rects, r3 <- rects
    , isTripod r (r1,r2,r3)
    , r1 <= r2, r1 <= r3    -- mild size reduction (first edge is minimal)
    , not (cancels r r1 r2) -- irreducibility condition
    , not (cancels r r2 r3)
    , not (cancels r r3 r1)
    , not (follows r r1 r2)
    , not (follows r r2 r3)
    , not (follows r r3 r1)
    ]
  where rects = rectangles r  

showTripod :: Tripod -> String
showTripod (r1,r2,r3)
  =  "(" ++ showRectangle r1
  ++ "," ++ showRectangle r2
  ++ "," ++ showRectangle r3 ++ ")"


-------------------------------------------------------------------
-- ** (Doubly) Open tripods

-- | open tripods, version 1
-- [R_0, (R_1,R_2)]
-- open between r_1,r_2
openTripods1 :: Relator -> [Tripod]
openTripods1 r
  = [ (r0,r1,r2)
    | r0 <- rects, (r1,r2) <- reducedRectanglePairs r -- r1 <- rects, r2 <- rects
    , follows r r2 r0
    , follows r r0 r1
    , r1 <= r0, r1 <= r2 -- size reduction (split edge is minimal)
    , not (follows r r1 r0) -- irreducibility conditions
    , not (follows r r0 r2)
    , not (cancels r r1 r0)
    , not (cancels r r0 r2)
    ]
  where
    rects = rectangles r

-- | open tripods, version 2
-- [(R_2,R_1), R_0]
-- open between r_1,r_2
openTripods2 :: Relator -> [Tripod]
openTripods2 r
  = [ (r2,r1,r0)
    | r0 <- rects, (r1,r2) <- reducedRectanglePairs r -- r1 <- rects, r2 <- rects
    , follows r r2 r0
    , follows r r0 r1
    , r2 <= r0, r2 <= r1 -- size reduction (split edge is minimal)
    , not (follows r r0 r2) -- irreducibility conditions
    , not (follows r r1 r0)
    , not (cancels r r0 r2)
    , not (cancels r r1 r0)
    ]
  where
    rects = rectangles r

-- | doubly open tripods
-- [[ (R_0, R_1), (R_0, R_2) ]]
-- open between r_0,r_1 and open between r_1,r_2
doublyOpenTripods :: Relator -> [Tripod]
doublyOpenTripods r
  = [ (r0,r1,r2)
    | (r0,r1) <- redRPs, r2 <- rects -- r0 <- rects, r1 <- rects, r2 <- rects
    , (r0,r2) `elem` redRPs -- reducibility conditions
    , not (follows r r1 r2)
    , not (cancels r r1 r2)
    , follows r r2 r1       -- following condition
    , r0 <= r1, r0 <= r2    -- size reduction (split edge is minimal)
    ]
  where
    rects  = rectangles r
    redRPs = reducedRectanglePairs r

-------------------------------------------------------------------
-- ** Collecting all pods

-- | wrapper type for all pods
data Pod
  = BP   Relator Bipod
  | TP   Relator Tripod
  | OTP1 Relator Tripod
  | OTP2 Relator Tripod
  | DOTP Relator Tripod
  deriving (Eq, Ord)

relator :: Pod -> Relator
relator (BP r _)   = r
relator (TP r _)   = r
relator (OTP1 r _) = r
relator (OTP2 r _) = r
relator (DOTP r _) = r

instance Show Pod where
  show (BP r x)            = showBipod x
  show (TP r x)            = showTripod x
  show (OTP1 r (r0,r1,r2)) = "["  ++ showRectangle r0  ++ "," ++ showBipod (r1,r2) ++ "]"
  show (OTP2 r (r2,r1,r0)) = "["  ++ showBipod (r2,r1) ++ "," ++ showRectangle r0  ++ "]"
  show (DOTP r (r0,r1,r2)) = "[[" ++ showBipod (r0,r1) ++ "," ++ showBipod (r0,r2) ++ "]]"


-- | the pod basis for the given relator
basis :: Relator -> [Pod]
basis r =  map (BP r)   (bipods r)
        ++ map (TP r)   (tripods r)
        ++ map (OTP1 r) (openTripods1 r)
        ++ map (OTP2 r) (openTripods2 r)
        ++ map (DOTP r) (doublyOpenTripods r)

-- data Num a => Vec a = Vec (M.Map Pod a)

-- | linear extension of maps defined on pods
linext :: Num a => (Pod -> a) -> [(Pod,a)] -> a
linext f = foldr (\(p,a) x -> a * f p + x ) 0

-------------------------------------------------------------------
-- * Basic constraints for the LP (A_0^\Q)
-------------------------------------------------------------------

-- | Count 1 if the arguments are equal; count 0 otherwise
countIfEqual :: (Num a, Eq b) => b -> b -> a
countIfEqual x y = case x == y of
                     True  -> 1
                     False -> 0


-------------------------------------------------------------------
-- ** Involution constraints

-- | every rectangle has to have a matching involution,
-- because each edge consists of two rectangles (with inverse orientaions)
involutionConstraint :: Fractional a => Rectangle -> Pod -> a
involutionConstraint rect p
  = occurrence rect p - occurrence (involution rect) p

-- | linear extension of @involutionConstraint@
involutionConstraintQ :: Fractional a => Rectangle -> [(Pod, a)] -> a
involutionConstraintQ rect 
  = linext (involutionConstraint rect) 

-- | (weighted) occurence of the given rectangle in the pods
occurrences :: Fractional a => Rectangle -> [(Pod, a)] -> a
occurrences rect = linext (occurrence rect)

-- | the occurence of a rectangle has to be weighted by 1/2 if it happens at a split edge (in OTP or DOTP)
occurrence :: Fractional a => Rectangle -> Pod -> a
occurrence rect (BP   r (r1,r2))     =        fromIntegral $ length $ filter ((==) rect) [r1,r2]
occurrence rect (TP   r (r1,r2,r3))  =        fromIntegral $ length $ filter ((==) rect) [r1,r2,r3]
occurrence rect (OTP1 r (r0,r1,r2))  = 1/2 * (fromIntegral $ length $ filter ((==) rect) [r0,r0,r1,r2])
occurrence rect (OTP2 r (r2,r1,r0))  = 1/2 * (fromIntegral $ length $ filter ((==) rect) [r0,r0,r1,r2])
occurrence rect (DOTP r (r0,r1,r2))  = 1/2 * (fromIntegral $ length $ filter ((==) rect) [r1,r2]) -- we are not allowed to count r0 here (this contribution is already taken care of by the OTPs)!

-------------------------------------------------------------------
-- ** open pod constraints
  
-- | fst/snd constraints at "open" pods
fstsndConstraint :: Num a => (Rectangle, Rectangle) -> Pod -> a
fstsndConstraint rp p = firstOccurrence rp p - sndOccurrence rp p 

-- | linear extension of @fstsndConstraint@
fstsndConstraintQ :: Num a => (Rectangle, Rectangle) -> [(Pod,a)] -> a
fstsndConstraintQ rp 
  = linext (fstsndConstraint rp)

-- | (weighted) occurence of the given rectangle pair in the first component
firstOccurrences :: Num a => (Rectangle, Rectangle) -> [(Pod,a)] -> a
firstOccurrences rp = linext (firstOccurrence rp)

firstOccurrence :: Num a => (Rectangle, Rectangle) -> Pod -> a
firstOccurrence rp (DOTP r (r0,r1,r2)) = countIfEqual rp (r0,r1) 
firstOccurrence rp (OTP2 r (r2,r1,r0)) = countIfEqual rp (r2,r1) 
firstOccurrence _ _ = 0            

-- | (weighted) occurence of the given rectangle pair in the second component
sndOccurrences :: Num a => (Rectangle, Rectangle) -> [(Pod,a)] -> a
sndOccurrences rp = linext (sndOccurrence rp)

sndOccurrence :: Num a => (Rectangle, Rectangle) -> Pod -> a
sndOccurrence rp (DOTP r (r0,r1,r2)) = countIfEqual rp (r0,r2)
sndOccurrence rp (OTP1 r (r0,r1,r2)) = countIfEqual rp (r1,r2)
sndOccurrence _ _ = 0

-------------------------------------------------------------------
-- * Weight functions/normalisation constraint for the LP
-------------------------------------------------------------------

-------------------------------------------------------------------
-- ** values on the pod basis

-- | lambda: the main contribution of the objective function;
-- corresponds to the sum of deg-2 over all vertices
lambda :: Num a => Pod -> a 
lambda (BP   r _)           = 0    -- (2-2) for each bipod 
lambda (TP   r (r1,r2,r3))  = 1    -- (3-2)
lambda (OTP1 r (r0,r1,r2))  = 1    -- we only count two of the four "half-edges" (-> -1 in total)
lambda (OTP2 r (r2,r1,r0))  = 1    -- we only count two of the four "half-edges" (-> -1 in total)
lambda (DOTP r (r0,r1,r2))  = 1    -- we only count the two half-edges not connected to the main vertex

-- | nu;
-- corresponds to the sum of all disk degrees
nu :: Fractional a => Pod -> a
nu p = 1/2 * 1/fromIntegral (length $ relator p) * nu' p 

-- each half-edge is counted from both sides (with the appropriate signs)
nu' :: Num a => Pod -> a
nu' (BP   r (r1,r2))     = s12 r r1 + s12 r r2
nu' (TP   r (r1,r2,r3))  = s12 r r1 + s12 r r2 + s12 r r3
nu' (OTP1 r (r0,r1,r2))  = s1 r r1 + s2 r r0 + s1 r r0 + s2 r r2
nu' (OTP2 r (r2,r1,r0))  = s1 r r1 + s2 r r0 + s1 r r0 + s2 r r2
nu' (DOTP r (r0,r1,r2))  = s1 r r1 + s2 r r2

-- | nuBar;
-- corresponds to 2 * sum of the absolute values of all disk degrees
nuBar :: Fractional a => Pod -> a
nuBar p = 2/fromIntegral (length $ relator p) * nuBar' p

-- each half-edge is counted once; thus, each edge is counted twice;
-- in total, we obtain |r| * the sum of absolute values of all disk degrees
nuBar' :: Num a => Pod -> a
nuBar' (BP   r bs)          = 2 * fromIntegral (length bs)
nuBar' (TP   r (r1,r2,r3))  = 3
nuBar' (OTP1 r (r0,r1,r2))  = 2
nuBar' (OTP2 r (r2,r1,r0))  = 2
nuBar' (DOTP r (r0,r1,r2))  = 1


-------------------------------------------------------------------
-- ** rational extensions

lambdaQ :: Fractional a => [(Pod, a)] -> a
lambdaQ = linext lambda

nuQ :: Fractional a => [(Pod, a)] -> a
nuQ = linext nu

nuBarQ :: Fractional a => [(Pod, a)] -> a
nuBarQ = linext nuBar


-------------------------------------------------------------------
-- ** the lallop functional

-- | lallop functional
lallopf :: Fractional a => Pod -> a
lallopf p = lambda p - nuBar p

-- | lallop functional, linear extension
lallopfQ :: Fractional a => [(Pod, a)] -> a
lallopfQ = linext  lallopf

{-
-- Charnes-Cooper transform
    lallop r = inf_x 2 * (lambda x - nuBar x)/(nu x)
                subject to nu x >= 1 and A_0 [which is homogeneous]
             = inf_y,t 2 * (lambda y - nuBar y)
                subject to nu y >= t
                           nu y == 1
                           t    >= 0 and A_0
             = inf_y 2 * (lambda y - nuBar y)
                subject to nu y == 1
                           A_0
-}

-------------------------------------------------------------------
-- ** passing the LP to z3

-- | passing the LP to z3
--
-- > optimize Lexicographic (lallopLP "abAB")
lallopLP :: Relator -> Goal
lallopLP r
  = do
     -- setting up the "variables":
     let b   =  basis r
     let xns =  map show b
     xs      <- mapM sReal xns
     let bxs =  zip b xs

     -- setting up the constraints:

     -- normalisation of powers/degrees
     constrain $ nuQ bxs .== 1

     -- A_0-constraints
     -- all coeffs >= 0
     mapM (\x -> constrain $ x .>= 0)
          xs       
     -- occurrences of rectangle pairs in fst component == occs ... in snd component
     mapM (\rp -> constrain $ fstsndConstraintQ rp bxs .== 0)
          (reducedRectanglePairs r)
     -- occurrences of rectangles == occurences of their involution
     mapM (\rect -> constrain $ involutionConstraintQ rect bxs .== 0)
          (rectangles r) -- could restrict to one half -- 2do!

     -- additional a-priori cut-down of the problem
     -- lallopf r <= simvol r <= 4 * scl r <= 4/4 * |r|
     constrain $ lallopfQ bxs .<= fromIntegral (length r)

     -- for debugging: (should eventually also have a-priori >= 0)
     constrain $ lallopfQ bxs .>= -12
     
     -- compute the optimal value via z3:
     minimize "lallop" $ lallopfQ bxs 


-------------------------------------------------------------------
-- * Example relators

-- **  the torus
ex_r  = "abAB"

-- | a solution for the torus ex_r
{- 
solv_r :: Fractional a => [(Pod,a)]
solv_r = [ (x,1), (y,1) ] 
  where
    a = ((3,Plus), (1,Plus))
    a' = involution a
    b = ((0,Plus), (2,Plus))
    b' = involution b
    x = OTP1 ex_r (a,  b,b')
    y = OTP2 ex_r (b,b', a')
-} 

-- ** surfaces of higher genus
ex_Sigma2 = "abABcdCD"
ex_Sigma3 = "abABcdCDefEF"
ex_Sigma4 = "abABcdCDefEFghGH"

-- ** further examples
ex_rr = "abABabAB"
ex_r2 = "abABaBBAbb"
ex_r3 = "abABaBBBAbbb"
ex_r4 = "abABaBBBBAbbbb"

-- ** a counterexample to the original conjecture
ex_v  = "aaaabABAbaBAAbAB"



-------------------------------------------------------------------
-- * For comparison: scl computation

-- |  scallop functional
scallopf :: Fractional a => Pod -> a
scallopf p = 1/4 * lambda p 

scallopfQ :: Fractional a => [(Pod, a)] -> a
scallopfQ = linext scallopf

-- |  passing the LP to z3
--
-- > optimize Lexicographic (scallopLP "abAB")
scallopLP :: Relator -> Goal
scallopLP r
  = do
     -- setting up the "variables":
     let b   =  basis r
     let xns =  map show b
     xs      <- mapM sReal xns
     let bxs =  zip b xs

     -- setting up the constraints:

     -- normalisation of powers/degrees
     constrain $ nuQ bxs .== 1

     -- A_0-constraints
     -- all coeffs >= 0
     mapM (\(_,x) -> constrain $ x .>= 0)
          bxs       
     -- occurrences of rectangle pairs in fst component == occs ... in snd component
     mapM (\rp -> constrain $ fstsndConstraintQ rp bxs .== 0)
          (rectanglePairs r)
     -- occurrences of rectangles == occurences of their involution
     mapM (\rect -> constrain $ involutionConstraintQ rect bxs .== 0)
          (rectangles r) -- could restrict to one half -- 2do!

     -- compute the optimal value via z3:
     minimize "scl" $ scallopfQ bxs 



